package responsemodel

import (
	"database/sql"
	"fmt"

	"gitlab.com/Pooji444/graphql-tool/util"
)

// AddResponseModel will be called by generate method
func AddResponseModel(tableName, aliasTableName string, db *sql.DB, model *[]string) {
	addResponseClassForTable(tableName, aliasTableName, db, model)
}

func addResponseClassForTable(tableName, aliasTableName string, db *sql.DB, model *[]string) {
	columns, dataTypes, err := util.GetColumnsAndDataTypes(tableName, db)
	util.CheckError(err)
	*model = append(*model, "export class "+aliasTableName+" {")

	for i, dataType := range dataTypes {
		classVariable := fmt.Sprintf("\n\t%s: %s", columns[i], util.GetTypeScriptType(dataType))
		*model = append(*model, classVariable)
	}

	*model = append(*model, "\n\tall"+aliasTableName+": "+aliasTableName+"[]")
	*model = append(*model, "\n}")
}
