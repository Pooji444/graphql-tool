package main

import (
	"fmt"

	"gitlab.com/Pooji444/graphql-tool/generate"
	"gitlab.com/Pooji444/graphql-tool/util"

	_ "github.com/lib/pq"
)

func main() {
	fmt.Println()
	schemaPath, modelPath, config := util.GetConfigAndInitialize()
	generate.Start(schemaPath, modelPath, config)
	fmt.Println()
}
