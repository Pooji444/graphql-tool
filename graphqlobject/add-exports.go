package graphqlobject

// AddModuleExports will be called by generate
func AddModuleExports(aliasTableName string, graphQL map[string][]string, order *[]string) {

	graphQL["exports"] = addModuleExports(aliasTableName)
	*order = append(*order, "exports")

}

func addModuleExports(aliasTableName string) []string {

	exports := []string{}
	mainObject := aliasTableName + "Type"

	if RootPresent == true {
		exports = append(exports, "module.exports = { "+mainObject+", "+aliasTableName+" }")
	} else {
		exports = append(exports, "module.exports = { "+mainObject+" }")
	}

	return exports

}
