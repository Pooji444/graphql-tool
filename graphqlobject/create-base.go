package graphqlobject

import (
	"database/sql"

	"gitlab.com/Pooji444/graphql-tool/util"
)

// CreateBase method will be called by generate method
func CreateBase(tableName, aliasTableName, dbName string, db *sql.DB, graphQL map[string][]string, order *[]string) {

	columns, dataTypes, err := util.GetColumnsAndDataTypes(tableName, db)
	util.CheckError(err)
	addNewGraphQLObjectType(tableName, aliasTableName, dbName, columns, dataTypes, graphQL, order)

}

func addNewGraphQLObjectType(tableName, aliasTableName, dbName string, columns, dataTypes []string, graphQL map[string][]string, order *[]string) {

	graphQL["basicImports"] = addBasicImports()
	graphQL["additionalImports"] = addAdditionalImports(dataTypes, columns)
	graphQL["basicType"] = addBasicType(tableName, aliasTableName, dataTypes, columns)
	*order = append(*order, "basicImports")
	*order = append(*order, "additionalImports")
	*order = append(*order, "basicType")

}

func addBasicImports() []string {
	basicImports := []string{}
	basicImports = append(basicImports, "const graphql = require('graphql')\n")
	basicImports = append(basicImports, "const dbConnection = require('../../db-connection')\n")
	basicImports = append(basicImports, "\n")
	return basicImports
}

func addAdditionalImports(dataTypes []string, columns []string) []string {
	additionalImports := []string{}

	keys := make(map[string]bool)
	timestampDatatype := false

	additionalImports = append(additionalImports, "const {\n")
	additionalImports = append(additionalImports, "\tGraphQLObjectType,\n")
	additionalImports = append(additionalImports, "\tGraphQLList,\n")

	for i, dataType := range dataTypes {
		graphQLType := util.GetGraphQLType(dataType, columns[i])
		if _, exists := keys[graphQLType]; !exists {
			keys[graphQLType] = true
			if graphQLType == "GraphQLDateTime" {
				timestampDatatype = true
			} else {
				additionalImports = append(additionalImports, "\t"+graphQLType+",\n")
			}
		}
	}

	additionalImports = append(additionalImports, "} = graphql\n")

	if timestampDatatype == true {
		additionalImports = append(additionalImports, "\nconst { GraphQLDateTime } = require('graphql-iso-date')\n")
	}
	additionalImports = append(additionalImports, "\n")

	return additionalImports

}

func addBasicType(tableName, aliasTableName string, dataTypes, columns []string) []string {
	basicType := []string{}

	objectName := aliasTableName + "Type"
	listOfType := "all" + aliasTableName

	basicType = append(basicType, "const "+objectName+" = new GraphQLObjectType({\n")
	basicType = append(basicType, "\tname: '"+aliasTableName+"',\n")
	basicType = append(basicType, "\tfields: () => ({"+"\n")
	for i := range columns {
		basicType = append(basicType, "\t\t"+columns[i]+": { type: "+util.GetGraphQLType(dataTypes[i], columns[i])+" },\n")
	}

	basicType = append(basicType, "\t\t"+listOfType+": "+listOfType+"\n")
	basicType = append(basicType, "\t})\n})")
	basicType = append(basicType, "\n")

	return basicType
}
