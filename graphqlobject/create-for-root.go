package graphqlobject

import (
	"database/sql"
	"fmt"

	"gitlab.com/Pooji444/graphql-tool/util"
)

// RootPresent is set to true if primary key is found, else false and is used accordingly by AddModuleExports
var RootPresent bool

// CreateTypeForRoot will be called by generate
func CreateTypeForRoot(db *sql.DB, dbName, tableName, aliasTableName string, graphQL map[string][]string, order *[]string) {

	graphQL["rootType"] = addRootType(db, dbName, tableName, aliasTableName)
	*order = append(*order, "rootType")

}

func addRootType(db *sql.DB, dbName, tableName, aliasTableName string) []string {
	rootType := []string{}

	column, dataType := getPrimaryColumnAndDataType(dbName, tableName, db)

	if column != "" {
		RootPresent = true
		graphQLDataType := util.GetGraphQLType(dataType, column)
		typeType := aliasTableName + "Type"
		queryType := "one"

		rootType = append(rootType, "\nconst "+aliasTableName+" = {\n")
		rootType = append(rootType, "\ttype: "+typeType+",\n")
		rootType = append(rootType, "\targs: { id: { type: "+graphQLDataType+" } },\n")
		rootType = append(rootType, "\tresolve(parentValue, args) {\n")
		rootType = append(rootType, "\t\tconst query = `SELECT * FROM "+dbName+"."+tableName+" where "+column+" = '${args."+column+"}'`\n")

		util.AddReturnMethod(&rootType, queryType)
		rootType = append(rootType, "\t}\n")
		rootType = append(rootType, "}\n")
		rootType = append(rootType, "\n")
	} else {
		RootPresent = false
		fmt.Println("\nNo Primary key found for table " + tableName + ", you need to manually add a type to put into root")
	}

	return rootType
}

func getPrimaryColumnAndDataType(dbName, tableName string, db *sql.DB) (string, string) {
	var column_name string
	var data_type string
	queryForPrimaryKey := `select ccu.column_name, col.data_type
							from information_schema.constraint_column_usage ccu
							inner join information_schema.table_constraints tc
							on ccu.constraint_name = tc.constraint_name
							and ccu.constraint_schema = tc.constraint_schema
							and ccu.table_name = tc.table_name
							inner join information_schema.columns col
							on col.column_name = ccu.column_name
							and col.table_name = ccu.table_name
							and col.table_schema = ccu.constraint_schema
							and tc.table_name = '` + tableName + `'
							and tc.table_schema = '` + dbName + `'
							and tc.constraint_type='PRIMARY KEY'`
	rows, err := db.Query(queryForPrimaryKey)
	util.CheckError(err)
	for rows.Next() {
		err = rows.Scan(&column_name, &data_type)
		util.CheckError(err)
	}
	return column_name, data_type
}
