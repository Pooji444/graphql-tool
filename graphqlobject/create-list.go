package graphqlobject

import (
	"gitlab.com/Pooji444/graphql-tool/util"
)

// CreateTypeList will be called by generate
func CreateTypeList(dbName, tableName, aliasTableName string, graphQL map[string][]string, order *[]string) {

	graphQL["listOfType"] = addListOfType(dbName, tableName, aliasTableName)
	*order = append(*order, "listOfType")

}

func addListOfType(dbName, tableName, aliasTableName string) []string {

	listOfType := []string{}
	listTypeName := "all" + aliasTableName
	listTypeType := aliasTableName + "Type"
	queryType := "many"

	listOfType = append(listOfType, "\nconst "+listTypeName+" = {\n")
	listOfType = append(listOfType, "\ttype: new GraphQLList("+listTypeType+"),\n")
	listOfType = append(listOfType, "\tresolve(parentValue, args) {\n")
	listOfType = append(listOfType, "\t\tconst query = `SELECT * FROM "+dbName+"."+tableName+"`\n")

	util.AddReturnMethod(&listOfType, queryType)

	listOfType = append(listOfType, "\t}\n")
	listOfType = append(listOfType, "}\n")
	listOfType = append(listOfType, "\n")

	return listOfType
}
