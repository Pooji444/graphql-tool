# Tool for generating GraphQL schema using GraphQLObjectType and response typescript models

 * Modify the config.yaml file according to your database configurations and list of tables for which you need to create schemas and models
 * Run `main.go` to generate the models and schemas.
 * These schemas and models can be found in your home directory.
 * Their path will be displayed in the output
