package generate

import (
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/Pooji444/graphql-tool/graphqlobject"
	"gitlab.com/Pooji444/graphql-tool/responsemodel"
	"gitlab.com/Pooji444/graphql-tool/util"
)

//ConstData will be set by Init method
type ConstData struct {
	db          *sql.DB
	graphQLFile *os.File
	modelFile   *os.File
}

var data ConstData

// Start will be called by main method to start the generation
func Start(schemaPath, modelPath string, config *util.Config) {

	data.db = util.GetDatabaseConnection(config.Database.Host, config.Database.Port, config.Database.User, config.Database.Password, config.Database.Dbname)

	if len(config.Database.Tables) == 0 {
		fmt.Println("Tables not provided in the config, getting all tables in database: " + config.Database.Dbname + "\n")
		config.Database.Tables = util.GetAllTables(config.Database.Dbname, data.db)
	}

	createGraphQLObject(config.Database.Tables, config.Database.Dbname, schemaPath)
	createResponseModel(config.Database.Tables, modelPath)
}

func createGraphQLObject(tables []string, dbName, filePath string) {

	for _, table := range tables {
		fmt.Println("Adding GraphQL schema for table: " + table)
		addGraphQLObject(table, dbName, filePath)
	}
}

func createResponseModel(tables []string, filePath string) {
	for _, table := range tables {
		fmt.Println("Adding Response model for table: " + table)
		addResponseModel(table, filePath)
	}
}

func addResponseModel(table, filePath string) {

	model := []string{}
	aliasTableName := util.RemoveUnderscoreAndCapitalize(table)
	data.modelFile = util.CreateNewFileForModel(table, filePath)
	responsemodel.AddResponseModel(table, aliasTableName, data.db, &model)

	for _, line := range model {
		data.modelFile.WriteString(line)
	}
}

func addGraphQLObject(table, dbName, filePath string) {

	aliasTableName := util.RemoveUnderscoreAndCapitalize(table)
	data.graphQLFile = util.CreateNewFileForGraphQLObject(table, filePath)
	order := []string{}
	graphQL := make(map[string][]string)
	graphqlobject.CreateBase(table, aliasTableName, dbName, data.db, graphQL, &order)
	graphqlobject.CreateTypeForRoot(data.db, dbName, table, aliasTableName, graphQL, &order)
	graphqlobject.CreateTypeList(dbName, table, aliasTableName, graphQL, &order)
	graphqlobject.AddModuleExports(aliasTableName, graphQL, &order)

	for _, orderValue := range order {
		graphQLValue := graphQL[orderValue]
		for _, line := range graphQLValue {
			data.graphQLFile.WriteString(line)
		}
	}
}
