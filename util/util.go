package util

import (
	"database/sql"
	"fmt"
	"os"
	"strings"
	"unicode"
)

// GetDatabaseConnection will be called by the generate method
func GetDatabaseConnection(host, port, user, password, dbname string) *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	CheckError(err)
	return db
}

// GetColumnsAndDataTypes will be called to create GraphQL object and model
func GetColumnsAndDataTypes(tableName string, db *sql.DB) ([]string, []string, error) {
	columns := []string{}
	dataTypes := []string{}

	var column_name string
	var data_type string
	sqlStatement := `SELECT data_type, column_name FROM information_schema.columns WHERE table_schema = 'marathi_katta' AND table_name = '` + tableName + `'`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		return columns, dataTypes, err
	}

	for rows.Next() {
		err = rows.Scan(&data_type, &column_name)
		if err != nil {
			return columns, dataTypes, err
		}
		columns = append(columns, column_name)
		dataTypes = append(dataTypes, data_type)
	}
	return columns, dataTypes, nil
}

// GetGraphQLType will be called to get GraphQL data type according to column's data type
func GetGraphQLType(dataType, column string) string {
	dataTypeName := strings.Split(dataType, " ")[0]
	if dataTypeName == "character" || dataTypeName == "uuid" || dataTypeName == "text" {
		return "GraphQLString"
	} else if dataTypeName == "integer" && column == "id" {
		return "GraphQLID"
	} else if dataTypeName == "integer" && column != "id" {
		return "GraphQLInt"
	} else if dataTypeName == "numeric" || dataTypeName == "double" {
		return "GraphQLFloat"
	} else if dataTypeName == "timestamp" {
		return "GraphQLDateTime"
	} else {
		return "GraphQLString"
	}
}

// CheckError is called to check if error exists, if it does, the program panics
// FIXME: Create error handling or directly print the error
func CheckError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// AddReturnMethod is called to add return method in a resolve method
func AddReturnMethod(graphQL *[]string, queryType string) {
	*graphQL = append(*graphQL, "\t\treturn dbConnection."+queryType+"(query)\n")
	*graphQL = append(*graphQL, "\t\t\t.then(data => {\n")
	*graphQL = append(*graphQL, "\t\t\t\treturn data\n")
	*graphQL = append(*graphQL, "\t\t\t})\n")
	*graphQL = append(*graphQL, "\t\t\t.catch(err => {\n")
	*graphQL = append(*graphQL, "\t\t\t\treturn 'The error is: ' + err\n")
	*graphQL = append(*graphQL, "\t\t\t})\n")
}

// GetTypeScriptType will be called while creating response model
func GetTypeScriptType(dataType string) string {
	dataTypeName := strings.Split(dataType, " ")[0]
	if dataTypeName == "character" || dataTypeName == "uuid" || dataTypeName == "text" {
		return "string"
	} else if dataTypeName == "integer" || dataTypeName == "numeric" || dataTypeName == "double" {
		return "number"
	} else if dataTypeName == "timestamp" {
		return "string"
	} else {
		return "string"
	}
}

// CreateNewFileForGraphQLObject will be called by the generate method before generating GraphQL objects
func CreateNewFileForGraphQLObject(tableName, filePath string) *os.File {
	file, err := os.Create("/" + filePath + "/" + tableName + ".js")
	CheckError(err)
	return file
}

// CreateNewFileForModel will be called by the generate method before generate new model
func CreateNewFileForModel(tableName, filePath string) *os.File {
	file, err := os.Create("/" + filePath + "/" + tableName + ".ts")
	CheckError(err)
	return file
}

// GetAllTables will be called by generate method if no tables are provided
func GetAllTables(dbName string, db *sql.DB) []string {
	tables := []string{}
	var table_name string

	sqlStatement := `SELECT table_name FROM information_schema.tables WHERE table_schema='` + dbName + `'`
	rows, err := db.Query(sqlStatement)
	CheckError(err)
	for rows.Next() {
		err = rows.Scan(&table_name)
		CheckError(err)
		tables = append(tables, table_name)
	}
	return tables
}

// RemoveUnderscoreAndCapitalize will convert a table name written in snake case to an alias table name written in pascal case and will be called once by the generate method
func RemoveUnderscoreAndCapitalize(tableName string) string {
	modifiedTableName := ""
	tableName = strings.Title(tableName)
	capitalize := false
	for _, character := range tableName {
		if character == '_' {
			capitalize = true
		} else if capitalize == true {
			modifiedTableName += string(unicode.ToTitle(character))
			capitalize = false
		} else {
			modifiedTableName += string(character)
		}
	}
	return modifiedTableName
}
