package util

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// Config is used to parse yaml and provide configuration information
type Config struct {
	Database struct {
		Host     string   `yaml:"host"`
		Port     string   `yaml:"port"`
		User     string   `yaml:"user"`
		Password string   `yaml:"password"`
		Dbname   string   `yaml:"dbname"`
		Tables   []string `yaml:"tables"`
	}
	Path string `yaml:"path"`
}

// GetConfigAndInitialize will be called to read yaml file
func GetConfigAndInitialize() (string, string, *Config) {
	fmt.Println("Reading configurations from yaml file: config.yaml")

	config := &Config{}

	file, err := os.Open("config.yaml")
	CheckError(err)
	defer file.Close()

	decode := yaml.NewDecoder(file)
	err = decode.Decode(&config)
	CheckError(err)
	schemaPath, modelpath := createDirectoriesIfTheyDoNotExist(config)

	return schemaPath, modelpath, config

}

func createDirectoriesIfTheyDoNotExist(config *Config) (string, string) {

	fmt.Println()
	path := ""
	if config.Path == "" {
		fmt.Println("Path is not provided, defaulting to: " + os.Getenv("HOME"))
		path = os.Getenv("HOME") + "/graphql-tool"
	} else {
		path = config.Path + "/graphql-tool"
	}

	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		fmt.Println("Creating directory graphql-tool under given path")
		CheckError(os.MkdirAll(path, 0755))
	}

	schemaPath := path + "/schema"
	modelPath := path + "/models"

	_, err = os.Stat(schemaPath)
	if os.IsNotExist(err) {
		CheckError(os.MkdirAll(schemaPath, 0755))
	}

	_, err = os.Stat(modelPath)
	if os.IsNotExist(err) {
		CheckError(os.MkdirAll(modelPath, 0755))
	}

	fmt.Println()
	fmt.Println("GraphQL Objects will be stored under path: " + schemaPath)
	fmt.Println("Models will be stored under path: " + modelPath)
	fmt.Println()

	return schemaPath, modelPath
}
